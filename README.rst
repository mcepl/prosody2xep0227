Minimalist script converting directory with stored Prosody_ data to
XEP-0227_ file.

.. _Prosody:
    http://prosody.im
.. _XEP-0227:
    http://xmpp.org/extensions/xep-0227.html
