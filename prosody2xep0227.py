#!/usr/bin/env python3
from __future__ import print_function, unicode_literals

import io
import argparse
import logging
try:
    from urllib import unquote
except ImportError:
    from urllib.parse import unquote
import os
import os.path
from pprint import pformat

import lua_table
try:
    import lxml.etree as ET
except ImportError:
    import xml.etree.ElementTree as ET


logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.INFO)
ROSTER_NS = 'jabber:iq:roster'
SERVER_NS = 'urn:xmpp:pie:0'
#ET.register_namespace(u'roster', ROSTER_NS)
#ET.register_namespace(u'server', SERVER_NS)

__version__ = '0.0.0'


def _xml_indent(elem, level=0):
    """Indent an XML element and its children.

    The element is modified in-place.

    Args:
        elem: the element to indent
        level: the current level of indentation

    """
    i = "\n" + level * "  "
    if len(elem) != 0:
        if not (elem.text and elem.text.strip()):
            elem.text = i + "  "
        for e in elem:
            _xml_indent(e, level + 1)
        if not (e.tail and e.tail.strip()):
            e.tail = i
    else:
        if level and not(elem.tail and elem.tail.strip()):
            elem.tail = i


class BaseObj(object):
    def __init__(self, parsed_file=None):
        self.parsed_file = parsed_file
        self.host_dir = None

    def _parse(self):
        item = None

        if os.path.exists(self.parsed_file):
            with io.open(self.parsed_file, 'r') as pfile:
                item = lua_table.parse(pfile)
                logging.debug('item = %s', pformat(item))

        return item

    def _parse_files(self, dir_name, class_name):
        items = []
        for item_file in os.listdir(os.path.join(self.host_dir, dir_name)):
            item_file = os.path.join(self.host_dir, dir_name, item_file)
            logging.debug('%s item_file = %s', dir_name, item_file)
            item = class_name(item_file)
            items.append(item)
        return items

    def as_xml(self):
        raise NotImplementedError('method as_xml() has not been implmeneted.')


class Host(BaseObj):
    """docstring for Host"""
    def __init__(self, host_dir):
        BaseObj.__init__(self)
        logging.debug('self = dir %s', dir(self))
        logging.debug('host_dir = %s', host_dir)
        self.users = []
        logging.debug('self = dir %s', dir(self))

        if os.path.exists(os.path.join(host_dir, 'accounts')):
            self.host_dir = host_dir

            self.users = self._parse_files('accounts', User)

    def __len__(self):
        return len(self.users)

    def as_xml(self):
        out = ET.Element('host')
        out.attrib['jid'] = unquote(os.path.basename(self.host_dir))

        for user in self.users:
            out.append(user.as_xml())

        return out


class User(BaseObj):
    def __init__(self, user_file):
        BaseObj.__init__(self, user_file)
        logging.debug('user_file = %s', user_file)
        self.roster = None

        roster_file = user_file.replace('accounts', 'roster')
        if os.path.exists(roster_file):
            self.roster = Roster(roster_file)

        self.user = self._parse()

    def as_xml(self):
        logging.debug('self.user = %s', self.user)
        out = ET.Element('user')
        out.attrib['name'] = os.path.splitext(
            os.path.basename(self.parsed_file))[0]
        out.attrib['password'] = self.user['password']

        if self.roster:
            logging.debug('roster:\n%s', ET.tostring(self.roster.as_xml()))
            out.append(self.roster.as_xml())

        return out


class Roster(BaseObj):
    def __init__(self, roster_file):
        BaseObj.__init__(self, roster_file)
        logging.debug('roster_file = %s', roster_file)

        rost = self._parse()
        logging.debug('rost:\n%s', rost)
        self.roster = {}
        for k in rost:
            if '@' in str(k):
                self.roster[k] = rost[k]
        logging.debug('self.roster:\n%s', self.roster)

    def as_xml(self):
        out = ET.Element('{%s}query' % ROSTER_NS)
        logging.debug("roster:\n%s", self.roster)
        for contact in self.roster:
            cur_cont = self.roster[contact]
            logging.debug('contact = %s', contact)
            cont_el = ET.Element('item')
            cont_el.attrib['jid'] = contact
            cont_el.attrib['subscription'] = cur_cont['subscription']

            if 'name' in cur_cont:
                cont_el.attrib['name'] = cur_cont['name']

            if 'ask' in cur_cont:
                cont_el.attrib['ask'] = cur_cont['ask']

            groups = cur_cont['groups']
            if groups:
                group_el = ET.Element('groups')
                group_el.text = ','.join(groups)
                cont_el.append(group_el)

            logging.debug('contact:\n%s', ET.tostring(cont_el))
            out.append(cont_el)

        logging.debug('roster:\n%s', ET.tostring(out))
        return out


def generate_xml(data):
    root = ET.Element('{%s}server-data' % SERVER_NS)

    for host in data:
        root.append(host.as_xml())

    _xml_indent(root)
    #print(ET.tostring(root, encoding='unicode').encode('utf8'))
    print(ET.tostring(root, encoding='unicode'))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('data_dir', action='append',
                        help='directories with data')
    args = parser.parse_args()

    data = []
    for serv_dir in args.data_dir:
        logging.debug('serv_dir = %s', serv_dir)
        serv_dir = os.path.expanduser(serv_dir)
        for host_dir in os.listdir(serv_dir):
            host_dir = os.path.abspath(os.path.join(serv_dir, host_dir))
            logging.debug('host_dir = %s', host_dir)
            logging.debug('host_dir = isdir %s', os.path.isdir(host_dir))
            if os.path.isdir(host_dir):
                host = Host(host_dir)
                if host:
                    data.append(host)

    generate_xml(data)


if __name__ == '__main__':
    main()
