from __future__ import print_function, unicode_literals
import unittest

from prosody2xep0227 import User


class TestProsodyParser(unittest.TestCase):
    def test_create_user(self):
        us = User('test/data/account.dat')

        self.assertIn('password', us.user)


class TestProsodyReader(unittest.TestCase):
    pass


if __name__ == '__main__':
    unittest.main()
