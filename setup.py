# coding: utf-8
from setuptools import setup
import prosody2xep0227
import os.path


def read(fname):
    with open(os.path.join(os.path.dirname(__file__), fname)) as inf:
        return "\n" + inf.read().replace("\r\n", "\n")

setup(
    name='prosody2xep0227',
    version=prosody2xep0227.__version__,
    description='Convert prosody data to XEP-0227 format',
    author='Matěj Cepl',
    author_email='mcepl@cepl.eu',
    url='https://gitlab.com/mcepl/prosody2xep0227/',
    long_description=read("README.rst"),
    keywords=['Prosody', 'Jabber', 'XMPP', 'XEP-0227'],
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    test_suite="test",
    install_requires=['lua_table']
)
